package org.example.config;

import org.example.dao.CourseDAO;
import org.example.dao.CourseDaoImpl;
import org.example.dao.StudentDAO;
import org.example.dao.StudentDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "org.example")
@EnableWebMvc
public class MvcConfiguration {

  @Bean
  public DataSource getDataSource(){
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
    dataSource.setUsername("postgres");
    dataSource.setPassword("root");

    return dataSource;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(){
    return new JdbcTemplate(getDataSource());
  }

 @Bean
  public StudentDAO getStudentDAO() {
    return new StudentDaoImpl(getDataSource());
  }

  @Bean
  public CourseDAO getCoursesDAO() {
    return new CourseDaoImpl(getDataSource());
  }
}
