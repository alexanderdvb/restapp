package org.example.dao;

import org.example.dao.rowMapprer.CourseRowMapper;
import org.example.model.Course;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

public class CourseDaoImpl implements CourseDAO{

  private JdbcTemplate jdbcTemplate;

  public CourseDaoImpl(DataSource dataSource){
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  @Override
  public List<Course> courseList(int id){

    String sql = "select " +
            "sc.idcs, " +
            "cs.namecs, " +
            "cs.durationcs, " +
            "cs.freeplacescs " +
            "from " +
            "students st, courses cs, studentscourses sc " +
            "where " +
            "st.idst= ? " +
            "and " +
            "st.idst=sc.idst " +
            "and " +
            "cs.idcs=sc.idcs";
    List<Course> coursesList = jdbcTemplate.query(sql, new Object[]{id}, (rs,  rowNum) -> {

      Course course = new Course();
      course.setCourseId(rs.getInt("idcs"));
      course.setCourseName(rs.getString("namecs"));
      course.setDuration(rs.getString("durationcs"));
      course.setFreePlaces(rs.getInt("freeplacescs"));

      return course;
    });
    return coursesList;
  }

  @Override
  public List<Course> list(){
    String sql = "SELECT * FROM courses";
    List<Course> list = jdbcTemplate.query(sql, (rs, rowNum) -> {

      Course course = new Course();
      course.setCourseId(rs.getInt("idcs"));
      course.setCourseName(rs.getString("namecs"));
      course.setDuration(rs.getString("durationcs"));
      course.setFreePlaces(rs.getInt("freeplacescs"));

      return course;
    });
    return list;
  }

  @Override
  public void addCourseToStudent(int studentId, int coursesId){
    String sql = "INSERT INTO studentscourses (idst, idcs) values('?','?')";
    jdbcTemplate.update(sql, studentId, coursesId);

  }

  @Override
  public void removeCourseFromStudent(int studentId, int courseId){
    String sql = "DELETE FROM studentscourses WHERE idcs=? and idst=?";
    jdbcTemplate.update(sql, courseId, studentId);
  }

  @Override
  public Course getCourse(int id) {
    String sql = "SELECT * FROM courses WHERE idcs=?";
    return jdbcTemplate.queryForObject(sql, new Object[]{id}, new CourseRowMapper());
  }

}
