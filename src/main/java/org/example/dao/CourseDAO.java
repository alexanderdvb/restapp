package org.example.dao;

import org.example.model.Course;

import java.util.List;

public interface CourseDAO {

  public List<Course> list();

  public List<Course> courseList(int id);

  public void addCourseToStudent(int studentId, int courseId);

  public void removeCourseFromStudent(int courseId, int studentId );

  public Course getCourse(int id);

}
