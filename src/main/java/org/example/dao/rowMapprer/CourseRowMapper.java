package org.example.dao.rowMapprer;

import org.example.model.Course;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CourseRowMapper implements RowMapper<Course> {

  @Override
  public Course mapRow(ResultSet rs, int rowNum) throws SQLException {

    Course course = new Course();
    course.setCourseId(rs.getInt("idcs"));
    course.setCourseName(rs.getString("namecs"));
    course.setDuration(rs.getString("durationcs"));
    course.setFreePlaces(rs.getInt("freeplacescs"));

    return course;
  }

}