package org.example.controller;

import org.example.dao.StudentDAO;
import org.example.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class StudentController {

  @Autowired
  private StudentDAO studentDAO;

  @GetMapping(value = "/all")
  public List<Student> studentList(){
    return studentDAO.list();
  }

  @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
  public ResponseEntity<Student> addNewStudent(@RequestBody Student student) {
    studentDAO.addStudent(student);
    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  @RequestMapping(value = "/editStudentPage/{id}", method = RequestMethod.GET)
  public ResponseEntity<Student> editStudentPage(@PathVariable(name = "id")int id){
    Student student = studentDAO.getStudent(id);
    return new ResponseEntity<>(student, HttpStatus.OK);
  }

  @RequestMapping(value = "/updateStudent", method = RequestMethod.PUT)
  public ResponseEntity<Student> updateStudent(@RequestBody Student student){
    studentDAO.editStudent(student);
    return new ResponseEntity<>(student, HttpStatus.OK);
  }

  @RequestMapping(value = "/deleteStudent/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Student> deleteStudent(@PathVariable(name = "id") int id) {
    studentDAO.deleteStudent(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
